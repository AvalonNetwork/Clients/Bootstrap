package com.avalon.bootstrap.api;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import com.google.gson.Gson;

import lombok.Getter;

public class Request {
	
	@Getter private static Request instance;
	
	@Getter private Response response;
	
	private String URL = "https://nxkzja.github.io/neziaa.com/avalon-dl.json";
	
	public Request() {
		instance = this;
	}
	
	public Response send() throws Exception {
	    String json = readUrl(URL);
		Gson gson = new Gson();      
		
		response = gson.fromJson(json, Response.class);
		return response;
	}
	
	private static String readUrl(String urlString) throws Exception {
	    BufferedReader reader = null;
	    try {
	        URL url = new URL(urlString);
	        reader = new BufferedReader(new InputStreamReader(url.openStream()));
	        StringBuffer buffer = new StringBuffer();
	        int read;
	        char[] chars = new char[1024];
	        while ((read = reader.read(chars)) != -1)
	            buffer.append(chars, 0, read); 

	        return buffer.toString();
	    } finally {
	        if (reader != null)
	            reader.close();
	    }
	}
	

}
