/**
 * @author Alexis 'Neziaa' � 2017-2018 - Bootstrap 
 *  @version 1.0.0-BETA
 *  @webURL  https://avalon.mc - https://neziaa.com
 *
 * This file is part of Bootstrap.
 * 
 * Bootstrap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bootstrap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Bootstrap. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package com.avalon.bootstrap;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import com.avalon.bootstrap.api.Request;
import com.avalon.bootstrap.api.Response;
import com.avalon.bootstrap.api.Response.DownloadMode;
import com.avalon.bootstrap.interfaces.Interface;
import com.avalon.bootstrap.utils.Directory;
import com.avalon.bootstrap.utils.FileDownloader;
import com.avalon.bootstrap.utils.Informations;
import com.neziaa.swinger.Swinger;
import com.neziaa.updater.BarAPI;
import com.neziaa.updater.Updaterz;

import lombok.Getter;
import net.wytrem.wylog.BasicLogger;
import net.wytrem.wylog.LoggerFactory;

public class Bootstrap {
	
	/**
	 * Logger 
	 */
	
	@Getter public static BasicLogger logger;
	
	/**
	 * Currently debugging?
	 */
	
	@Getter public static boolean debug;

	public static void main(String[] args) {
		
		/**
		 * Logger
		 */
		
		logger = LoggerFactory.getLogger(Informations.getName());
		
		/**
		 * Swinger
		 */
		
		Swinger.setSystemLookNFeel();
		Swinger.setResourcePath(Informations.getTexturePath());
		
		/**
		 * Currently debugging?
		 */
		
		for(String argument: args)
			if(argument == "debug")
				debug = true;
		
		/**
		 * Interface
		 */
		
		new Interface();
	}
	
	public static void update() throws Exception {
		
		/**
		 * Request to JSON
		 */
		
		Request request = new Request(); 
		
		/**
		 * Response ORM
		 */
		
		Response response = request.send();
		
		/**
		 * Active?
		 */
		
		if(response.isActive()) {
			if(response.getDownloader() == DownloadMode.SUpdate) {
				
				/**
				 * Create updater Object.
				 */
				
				Updaterz updater = new Updaterz(response.getUri(), new Directory().get());
				
				/**
				 * Thread
				 */
				
		        Thread updateThread = new Thread() {
		        	
		            private int val;
		            private int max;

		            @Override
		            public void run() {
		                while (!this.isInterrupted()) {
		                    this.val = (int)(BarAPI.getNumberOfTotalDownloadedBytes() / 1000);
		                    this.max = (int)(BarAPI.getNumberOfTotalBytesToDownload() / 1000);
		                    
		                    Interface.getInstance().getBar().setValue(val);
		                    Interface.getInstance().getBar().setMaximum(max);
		                }
		            }
		        };
		        
		        updateThread.start();
		        updater.start();
				updateThread.interrupt();
				
			} else {

				/**
				 * Create downloader Object
				 */
				
				FileDownloader downloader = new FileDownloader(new URL(response.getUri()), new File(new Directory().get() + "/launcher" + ".jar")); 
				
				/**
				 * Launch download
				 */

				downloader.run();
			}
		}
	}
	
	public static void launch() throws IOException {

		/**
		 * Launch Launcher.jar
		 */
		
		logger.info("Launching...");
		
		Desktop.getDesktop().open(new File(new Directory().get(), "launcher.jar"));
		
		logger.info("Launched.");
		
		/**
		 * Exit Bootstrap.
		 */
		
		System.exit(0);
	}
}
