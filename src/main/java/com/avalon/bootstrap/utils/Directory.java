/**
 * @author Alexis 'Neziaa' � 2017-2018 - Bootstrap 
 *  @version 1.0.0-BETA
 *  @webURL  https://avalon.mc - https://neziaa.com
 *
 * This file is part of Bootstrap.
 * 
 * Bootstrap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bootstrap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Bootstrap. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package com.avalon.bootstrap.utils;

import java.io.File;

public class Directory {
	
	/**
	 * Return {@link File} where Launcher need to be installed.
	 * @return	File
	 */
	
	public File get() {
		String os = System.getProperty("os.name").toLowerCase();
		String name = Informations.getName();
		String version = "Launcher";
		
		if (os.contains("win")) return new File(System.getProperty("user.home") + "\\AppData\\Roaming\\" + name + "/" + version);
		if (os.contains("mac")) return new File(System.getProperty("user.home") + "/Library/Application Support/" + name + "/" + version);

		return new File(System.getProperty("user.home") + "/" + name + "/" + version);
	}
}
