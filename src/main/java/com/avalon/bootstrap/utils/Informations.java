/**
 * @author Alexis 'Neziaa' � 2017-2018 - Bootstrap 
 *  @version 1.0.0-BETA
 *  @webURL  https://avalon.mc - https://neziaa.com
 *
 * This file is part of Bootstrap.
 * 
 * Bootstrap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bootstrap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Bootstrap. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package com.avalon.bootstrap.utils;

import lombok.Getter;

public class Informations {

	/**
	 * Launcher Informations
	 */
	
	@Getter private static String name = "Avalon";
	
	/**
	 * Launcher Dimensions
	 */
	
	@Getter private static Integer width  = 600;
	@Getter private static Integer height = 130;
	
	/**
	 * Launcher Versions
	 */
	
	@Getter private static String version = "BETA";
	@Getter private static String completeVersion = "2.0-BETA";
	
	/**
	 * Launcher Utilities
	 */
	
	@Getter private static String texturePath = "/com/avalon/bootstrap/textures/";
}
