/**
 * @author Alexis 'Neziaa' � 2017-2018 - Bootstrap 
 *  @version 1.0.0-BETA
 *  @webURL  https://avalon.mc - https://neziaa.com
 *
 * This file is part of Bootstrap.
 * 
 * Bootstrap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bootstrap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Bootstrap. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package com.avalon.bootstrap.utils;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import com.avalon.bootstrap.interfaces.Interface;

public class FileDownloader {
	

    /**
     * The URL of the file to download
     */
    private URL fileUrl;

    /**
     * The destination file
     */
    private File dest;

    /**
     * Simple constructor
     *
     * @param fileUrl
     *            The URL of the file to download
     * @param dest
     *            The destination file
     */
    public FileDownloader(URL fileUrl, File dest) {
        this.fileUrl = fileUrl;
        this.dest = dest;
    }

    public void run() {
        // Making the parent folders of the destination file
        dest.getParentFile().mkdirs();

        try {
            // Creating the connection
            HttpURLConnection connection = (HttpURLConnection) fileUrl.openConnection();

            // Adding some user agents
            connection.addRequestProperty("User-Agent", "Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36");

            // Creating the data input stream
            DataInputStream dis = new DataInputStream(connection.getInputStream());

            // Transfering
            byte[] fileData = new byte[connection.getContentLength()];
            
            // ProgressBar 
            Interface.getInstance().getBar().setMaximum(connection.getContentLength());
            
            int x;
            for (x = 0; x < fileData.length; x++)  {
                Interface.getInstance().getBar().setValue(Interface.getInstance().getBar().getValue() + 1);
                fileData[x] = dis.readByte();
            }

            // Closing the input stream
            dis.close();

            // Writing the file
            FileOutputStream fos = new FileOutputStream(dest);
            fos.write(fileData);
            
            // Closing the output stream
            fos.close();
        } catch (IOException e) {
            // If it failed printing a warning message
        	System.out.println("File " + fileUrl + " wasn't downloaded, error: " + e);
        }
    }


}
