/**
 * @author Alexis 'Neziaa' � 2017-2018 - Bootstrap 
 *  @version 1.0.0-BETA
 *  @webURL  https://avalon.mc - https://neziaa.com
 *
 * This file is part of Bootstrap.
 * 
 * Bootstrap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bootstrap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Bootstrap. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package com.avalon.bootstrap.interfaces;

import java.awt.Color;
import java.io.IOException;

import javax.swing.JFrame;

import com.avalon.bootstrap.Bootstrap;
import com.avalon.bootstrap.resources.ResourceBackground;
import com.avalon.bootstrap.utils.Informations;
import com.avalon.bootstrap.utils.MouseManager;
import com.neziaa.swinger.Swinger;
import com.neziaa.swinger.colored.ColoredBar;
import com.neziaa.swinger.textured.TexturedButton;

import lombok.Getter;
import net.wytrem.wylog.BasicLogger;

@SuppressWarnings("serial")
public class Interface extends JFrame {
	
	/**
	 * Instance
	 */
	
	@Getter private static Interface instance;
	
	/**
	 * Logger
	 */
	
	@Getter private BasicLogger logger;
	
	/**
	 * ProgressBar
	 */
	
	@Getter private ColoredBar bar;

	public Interface() {
		
		/**
		 * Instance
		 */
		
		instance = this;
		
		/**
		 * Logger
		 */
		
		logger = Bootstrap.getLogger();
		
		/**
		 * Display
		 */
		
		this.setTitle(Informations.getName());
		this.setSize(Informations.getWidth(), Informations.getHeight());
		this.setUndecorated(true);
		this.getContentPane().setBackground(new Color(255, 255, 255));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setLocationRelativeTo(null);
		
		logger.info("Ready to displaying.");
		
		/**
		 * Setup favicon
		 */
		
		this.setIconImage(Swinger.getResource("favicon.png"));
		logger.info("Favicon setup.");
		
		/**
		 * Setup background
		 */
		
		ResourceBackground background = new ResourceBackground(Swinger.getResource("bg.png"), Informations.getWidth(), Informations.getHeight());
		this.setContentPane(background);
		logger.info("Background drawed.");
		
		/**
		 * Loading MouseManagers.
		 */
		
		MouseManager mouseManager = new MouseManager(this);
		this.addMouseListener(mouseManager);
		this.addMouseMotionListener(mouseManager);
		logger.info("MouseManager loaded.");
		
		/**
		 * Logo
		 */
		
		TexturedButton logo = new TexturedButton(Swinger.getResource("logo.png"), Swinger.getResource("logo.png"));
		logo.setBounds(225, 10, 125, 75);
		add(logo);
		
		/**
		 * ProgressBar
		 */
		
		this.bar = new ColoredBar(Swinger.getTransparentInstance(Color.BLACK, 100), new Color(51, 128, 162));
		this.bar.setBounds(80, 95, 440, 20);
		add(this.bar);
		
		/**
		 * Display Interface
		 */
		
		this.setVisible(true);
		
		/**
		 * Update
		 */
		
		try {
			
			Bootstrap.update();
			
		} catch (Exception e) {
			
			logger.error("Failed to Update.", e);
			
		}
		
		/**
		 * Launch
		 */
		
		try {
			
			Bootstrap.launch();
			
		} catch (IOException e) {
			
			logger.error("Failed to Update.", e);
			
		}
	}
}
