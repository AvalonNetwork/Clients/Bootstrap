/**
 * @author Alexis 'Neziaa' � 2017-2018 - Bootstrap 
 *  @version 1.0.0-BETA
 *  @webURL  https://avalon.mc - https://neziaa.com
 *
 * This file is part of Bootstrap.
 * 
 * Bootstrap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bootstrap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Bootstrap. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package com.avalon.bootstrap.resources;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ResourceBackground extends JPanel {
	
	/**
	 * Background Image
	 */
	private Image background;
	
	/**
	 * Constructor 
	 * @param background	Name of the ressource.
	 * @param width			Width of the Launcher or the Background  Width's
	 * @param height		Height of the Launcher or the Background Height's
	 */
	
	public ResourceBackground(Image image, int width, int height) {
		
		this.background = image;
		
		Dimension size = new Dimension(width, height);
		
		this.setSize(size);
		this.setPreferredSize(size);
		this.setMaximumSize(size);
		this.setMinimumSize(size);
		this.setLayout(null);
	}
	
	/**
	 * Draw the background.
	 */
	
	public void paintComponent(Graphics graphics) {
		graphics.drawImage(this.background, 0, 0, this.getWidth(), this.getHeight(), this);
	}
}
